#include <iostream>
#include <cassert>

#include "cat.hpp"
#include "list.hpp"
#include "node.hpp"

using namespace std;


int main() {

      cout << "Cat Wramgler Unit Tests:" << endl;
      Node node;
      DoubleLinkedList list;

      Node* pCat1 = new Node;
      Node* pCat2 = new Node; 
      Node* pCat3 = new Node;
      Node* pCat4 = new Node;
      Node* pCat5 = new Node;

      assert( list.get_first() == nullptr );
      assert( list.get_last() == nullptr );

      //PushFront + PushBack
      list.push_front(pCat1);
      list.push_front(pCat2);
      list.push_back(pCat3);
      list.insert_after(pCat1, pCat4);
      list.insert_before(pCat1, pCat5);

      
      assert( list.get_next(pCat2) != pCat3 ); //head pointers dont have null nexts
      assert( list.get_next(pCat3) == nullptr ); //tail pointers have null nexts
      assert( list.get_prev(pCat3) != pCat2 );
      assert( list.get_next(pCat1) == pCat4 );  //check insert after function
      assert( list.get_next(pCat2) == pCat5 ); //check insert before function

      assert( list.validate() );
      assert( list.get_last() == pCat3 );
      assert( list.get_first() == pCat2 );


      //PopFront + PopBack
      list.pop_front();
      list.pop_back();

      if ( list.empty() )
         cout << "List is empty!" << endl;
      else
         cout << "List is not empty!" << endl;

      assert( list.validate() );
      cout << "List has " << list.size() << " elements!" << endl;


}
