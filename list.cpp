///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.cpp
/// @version 1.0
///
/// Functionality of class SingleLinkedList Members
///
/// @author Matthew Kouchi <@mkouchi2@hawaii.edu>
/// @brief  Lab 08a - CatWrangler - EE 205 - Spr 2021
/// @date   13_APR_2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>

#include "list.hpp"

using namespace std;


DoubleLinkedList::DoubleLinkedList() 
{
   head = nullptr;
   tail = nullptr;
   count = 0;
}

const bool DoubleLinkedList::isSorted() const {
   if (count<=1)
      return true;
   for(Node* i=head ; i->next != nullptr ; i=i->next){
      if (*i > *i->next)
         return false;
   }
   return true;
}

void DoubleLinkedList::swap (Node* node1, Node* node2)
{
   if ( node1 == node2 )
      return;
   
   if ( node1->next == node2 ) { //adjacent nodes
      node1->next = node2->next;
      node2->prev = node1->prev;
      
      if ( node1->next != nullptr )
         node1->next->prev = node1;
      if ( node2->prev != nullptr )
         node2->prev->next = node2;
      
      node2->next = node1;
      node1->prev = node2;
   } else {
        Node* tempNode1 = node2->prev;
        Node* tempNode2 = node2->next;

        node2->prev = node1->prev;
        node2->next = node1->next;

        node1->prev = tempNode1;
        node1->next = tempNode2;

        if (node2->next != nullptr)
            node2->next->prev = node2;
        if (node2->prev != nullptr)
            node2->prev->next = node2;

        if (node1->next != nullptr)
            node1->next->prev = node1;
        if (node1->prev != nullptr)
            node1->prev->next = node1;
    } 
}

void DoubleLinkedList::insertionSort() {
   for ( Node* i=head; i->next != nullptr ; i = i->next ) {
      if ( *i > *i->next )
         return;
      else {
         DoubleLinkedList::swap(i, i->next);
      }
   }
}

bool DoubleLinkedList::validate() const 
{
   if ( head == nullptr ) {
      assert( tail == nullptr );
      assert( count == 0 );
   } else {
      assert( tail != nullptr );
      assert( count !=0 );
   }

   if ( tail == nullptr ) {
      assert( head == nullptr );
      assert( count == 0 );
   } else {
      assert( head != nullptr);
      assert( count != 0);
   }

   if ( head != nullptr && tail == head ) {
      assert( count == 1 );
   }

   unsigned int fowardCount = 0;
   Node* currentNode = head;
   while ( currentNode != nullptr ) {
      fowardCount++;
      if ( currentNode->next != nullptr ) {
         assert( currentNode->next->prev == currentNode );
   }
      currentNode = currentNode->next;
   }
   assert( count == fowardCount );

   unsigned int backwardCount = 0;
   currentNode = tail;
   while ( currentNode != nullptr ) {
      backwardCount++;
      if ( currentNode->prev != nullptr ) {
         assert( currentNode->prev->next == currentNode );
      }
      currentNode = currentNode->prev;
   }
   assert( count == backwardCount );

   return true;
}

const bool DoubleLinkedList::isIn( Node* aNode ) const 
{
   Node* currentNode = head;
   while ( currentNode != nullptr) {
      if ( aNode == currentNode )
         return true;
      currentNode = currentNode->next;
   }
   return false;
}


void DoubleLinkedList::push_back( Node* newNode ) 
{
   if ( newNode == nullptr )
      return;

   if ( tail != nullptr ) {
      newNode->prev = tail;
      newNode->next = nullptr;
      tail->next = newNode;
      tail = newNode;
   
   } else {
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
   }
   count++;
   validate();
}

Node* DoubleLinkedList::pop_front() 
{
   if ( head == nullptr ) //if list is empty
      return nullptr; 
   
   else {
      head = head->next;
      if ( head == nullptr ) { //if list is only one element
         count--;
         validate();
         return nullptr;
   }  else {      
         head->prev = nullptr; //normal case
         count--;
         validate();
         return head;
      }
   }
}

void DoubleLinkedList::insert_after( Node* currentNode, Node* newNode )
{
   if ( currentNode == nullptr && head == nullptr ) {
      push_front( newNode );
      return;
   }

   if ( currentNode != nullptr && head == nullptr ) {
      assert( false ); 
   }

   if ( currentNode == nullptr && head != nullptr ) {
      assert( false );
   }
   assert( currentNode != nullptr && head != nullptr );
   assert( isIn( currentNode ));
   assert( !isIn( newNode ));

   if ( tail != currentNode ) {
      newNode->next = currentNode->next;
      newNode->prev = currentNode;
      currentNode->next = newNode;
      newNode->next->prev = newNode;
      
   } else {
      push_back( newNode );
   }
   count++;
   validate();
}

void DoubleLinkedList::insert_before( Node* currentNode, Node* newNode )
{
   if ( currentNode == nullptr && head == nullptr ) {
      push_front( newNode );
      return;
   }

   if ( currentNode != nullptr && head == nullptr ) {
      assert( false );
   }

   if ( currentNode == nullptr && head != nullptr ) {
      assert( false );
   }

   assert( currentNode != nullptr && head != nullptr );
   assert( isIn( currentNode ));
   assert( !isIn( newNode ));

   if ( tail != currentNode ) {
      newNode->prev = currentNode->prev;
      newNode->next = currentNode;
      currentNode->prev = newNode;
      newNode->prev->next = newNode;

   } else {
      push_back( newNode );
   }
   count++;
   validate();
}


Node* DoubleLinkedList::get_last() const 
{
   return tail;
}


Node* DoubleLinkedList::get_prev( const Node* currentNode ) const
{
   return currentNode->prev;
}



