///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file list.hpp
/// @version 1.0
///
/// 
///
/// @author Matthew Kouchi <@mkouchi2@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   30_MAR_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include "node.hpp"

using namespace std;

   
   class DoubleLinkedList {
     
      friend class Node;
      friend class Queue; 

      protected:
        Node* head;
        Node* tail;
        unsigned int count;

      public:
         //Method Declarations
         DoubleLinkedList(); 
         const bool empty() const;
         void push_front( Node* newNode);
         Node* pop_front();
         Node* get_first() const;
         Node* get_next( const Node* currentNode ) const;
         void push_back( Node* newNode );
         Node* pop_back();
         Node* get_last() const;
         Node* get_prev( const Node* currentNode ) const;
         void insert_after( Node* currentNode, Node* newNode );
         void insert_before( Node* currentNode, Node* newNode );
         const bool isIn( Node* aNode ) const;
         bool validate() const;
         unsigned int size() const;
   
         void swap( Node* node1, Node* node2 );
         const bool isSorted() const;
         void insertionSort();
   };
   
inline unsigned int DoubleLinkedList::size() const 
{
      return count;
}

inline const bool DoubleLinkedList::empty() const 
{
   if ( head == nullptr ) 
      return true;
   else
      return false;
}

inline void DoubleLinkedList::push_front( Node* newNode ) 
{
   if ( newNode == nullptr )
      return;
   
   if ( head != nullptr ) {
      newNode->next = head;
      newNode->prev = nullptr;
      head->prev = newNode;
      head = newNode;
   
   } else {
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
   }
   count++;
   validate();
}

inline Node* DoubleLinkedList::pop_back()
{
   if ( tail == nullptr )
      return nullptr;
   
   else {
      tail = tail->prev;
      if ( tail == nullptr ) {
         count--;
         validate();
         return nullptr;
   }  else {
         tail->next = nullptr;
         count--;
         validate();
         return tail;
   }
   }
}

inline Node* DoubleLinkedList::get_first() const 
{
   return head;
}

inline Node* DoubleLinkedList::get_next( const Node* currentNode ) const 
{
   return currentNode->next;
}



